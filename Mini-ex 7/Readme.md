**AP2019 Mini-ex 7**

I collaborated with Annebel (@annebeltz). If you want the see the index and sketch file, go to her gitlab account please.

We have created a generative program where we have borrowed inspiration from Winnies code. When deciding on the colors, song and the text we wanted to give the program an 90's vibe. 

We tried to change the code and implement new features such as music ([It doesnt work in the browser, song used in the program](http://freemusicarchive.org/music/Kai_Nobuko/Sincerity_Is_The_Key/4_-_16_-_Kai_Nobuko_-_Computer_Sounds)) and text.

Our generative program may look like a chaotic system, as Langton said: "*give the appearance of being random and unpredictable*"..."*even when the rules governing their behavior are completely deterministic*" (Langton, 1986, p124). We created 3 rules that the program is based on and it loops over and over again - So it never stops running. When talking about "Generative art", it is an art practice where the artist hands over her control to a system that can function on it's own/autonomously. The system will contribute to or create art on its own. The type of systems can range from natural language instructions to mathematical operations or to computer programs and biological processes. 

When talking about the differences between generative art and automatism the important question is "Who is the Author?"" Is it the artist/programmer or the syhstem? Who makes the choices and who is responsible for the artwork? As Marius Watz said/repeated during his talk "Beautiful rules: Generative models of creativity": "*Generative art refers to any art practice where the artist uses a system, such as a set of natural language rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art*." ([Watz, 2007](https://vimeo.com/26594644))



**The rules in our program:**


*  Rule no. 1: The yellow dot starts from the right in the grid and moves to the left. When the yellow dot hits the left side of the canvas, it will automatically continue on the right side of the canvas.

*  Rule no. 2: This continues till the yellow dot hits its own axis, where the dot will become purple and turn right 90 degrees and continue in the new axis. This continues till...

*  Rule no. 3:...the yellow dot hits the top of the canvas (y-1), where it turns -90 degrees and becomes purple. This continues in the same pattern with the same rules. The program will continuesly loop over and over again.



[Runme](https://glcdn.githack.com/Annebeltz/ap2019/raw/master/Mini_ex7/index.html?fbclid=IwAR1sZ2kWTduUgbXqMXIPH05fLXuckwvHfiX2Wwt37CcCSr7BgXPWiOI_zEY)
![Screenshot](skærmbillede1.png)
![Screenshot](skærmbillede2.png)
![Screenshot](kode1.png)
![Screenshot](kode2.png)
![Screenshot](kode3.png)
![Screenshot](kode4.png)

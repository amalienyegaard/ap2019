//kode til mandag d. 11 marts
let song;
let cloud;
let tardis;
let angle = 0;

function preload() {
 song = loadSound('dwt.mp3');
 cloud = loadImage('cloud.jpg')
 tardis = loadImage('tardis2.jpg')
}

function setup() {
  createCanvas(600, 500);
  song.play();
  angleMode(DEGREES);

}

function draw() {

//background image
push();
  scale(0.7);
  image(cloud, 0, 0);
pop();

//lightning
push();
 noStroke();
 fill(random(200), 80, 2, 90);
 rect(0, 0, width, height);
 pop();


//tardis
  translate(350, 200);
  scale(0.6);
  rotate(angle);
  image(tardis, 0, 0, 106, 150);

angle = angle + 6;

}
//turn and on the music
function mousePressed() {
  if (song.isPlaying()) {
    song.stop();
  } else {
    song.play();
}
}

**AP2019 Mini-ex 5**

I chose to re-visit my first program in this class, [Mini-ex 1 ](https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%201/empty-example/index.html).
I had drawn inspiration from the TV show Doctor Who, which I have always been very fond of. I liked the way my program was spinning and rotating in 3D at the time of the creation. I wanted to add music and I wanted it to look a bit more like the Tardis and the background to look more like space. So when I started to re-examine the program and when I thought of making adjustments, I wanted to implement the above. 

I started out with continuing to make the program in 3D, but there was obstacles I do not (YET) know how to handle. First of, how you can use images in 3D was quite challening. I found a picture online of the Tardis and then created a **texture** to a box. I worked pretty well and it looked like my first conceptuel idea. Then I stumbolled upon some difficulties. I wanted the background to be an image to, and I tried to create a plane with the image on. After experimenting with that for a reasonable amount of time, I then wanted to see how I could do the above in 2D. Let's just say, it was a lot more manageable. In 2D I made a background and the image of clouds to cover it. On top of those to layers, I created a third - lighting, to make it look more spacey-wormhole alike. This was also the first time I experimented with sound. I created a slider in the first 3D program but decided not to keep it. Instead I chose that with mousePressed, the user can turn on/off the music.
I am delighted to see how much I have learned since making my first program and I look forward to be learning more. 

[Mini-ex 5](https://glcdn.githack.com/amalienyegaard/ap2019/raw/master/Mini-ex%205/empty-example/index5.html)
Please note, that you have to press the screen with your mouse to activate the music. 
![Screenshot](screenshot.png) 

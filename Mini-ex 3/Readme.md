**AP2019 Mini-ex 3**

**What are the time-related syntaxes/functions that you have used in your program? and why you use in this way? How is time being constructed in computation?**


*  I thought a lot about the rotate syntax and how I could use that to create some sort of time relation to the user. 
*  I couldn't really make it work, the way I wanted and I chose to use the timerelated syntaxes as frameCount and frameRate.  
*  The three rotating objects and the text below should stress/confuse the user of my program.
*  My program's theme is space. To me, space is timeless and something that I don't know much about. 
*  Space is big, confusing and infinite. So is time.

I had a lot of difficulty with my program because I wanted the shapes to be in 3D and a lot of the syntaxes can't be used with them because of WEBGL in createCanvas. Luckily I had a super helpful classmate (Adam) that showed me how to use text in my program. 


**Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction, what do you think a throbber tells us, and/or hides, about? How might we think about this remarkable throbber icon differently?**

The first throbber example that came to mind was the "waiting in line"-throbber on Ticketmaster. Last week I was bying concert tickets on the website and waited in line to choose the tickets that I wanted (if there was any left). When the tickets are available, you get redirected to a waiting page with a throbber that indicates your position in the queue. The throbber is a circle, where depending on how long you have left a blue colour fills out the circle. In the middle or at the top of the circle, there is a number that show either the approximated waiting time or your number in the line. The throbber works very well for me in this case because I get an indication of how far I am in the process (in this case) of bying tickets. Of course there is a lot of people "in-line" and it is nice to get an ETA to the ticket website.

[Runme](https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%203/empty-example/index3.html)

![Screenshot](skærmbillede.png) 


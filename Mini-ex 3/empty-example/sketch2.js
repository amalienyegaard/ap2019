//mini-ex2 monday 25.02.18
var myFont;
function preload(){
  myFont = loadFont('AppleGothic.ttf')

}
function setup() {
createCanvas(windowWidth, windowHeight, WEBGL);
frameRate(80);
}

function draw() {
var textColor = color(255, 255, 255);
background(0,0,55);

//text at the top
push();
 fill(textColor)
 textFont(myFont);
 textSize(20);
 textAlign(CENTER, CENTER);
 text("Downloading...*",0,240)
pop();

//text in the middle
push();
 fill(textColor)
 textFont(myFont);
 textSize(13);
 textAlign(CENTER, CENTER);
 text("*It can take up to 1 light year",0,270)
pop();

//text in the middle
push();
 fill(textColor)
 textFont(myFont);
 textSize(12);
 textAlign(CENTER, CENTER);
 text("**so about 365.25 days",0,290)
pop();

 //text at the bottom
push();
 fill(textColor)
 textFont(myFont);
 textSize(10);
 textAlign(CENTER, CENTER);
 text("***or 31557600 seconds",0,310)
pop();


//planet
push();
fill(220,130,225);
noStroke()
sphere(130);
rotateY(frameCount * 0.08);
//ring
fill(30,30);
torus(140,12,24,16);
pop();


//spaceship2 green
push();
rotateZ(frameCount * 0.02);
noStroke();
fill(random(255),250,0);
translate(-55, 210);
cone(20,14,23,11);
ellipsoid(8, 8, 8);
pop();

//spaceship1 grey
push();
rotateZ(frameCount * 0.045);
noStroke();
fill(random(203),203);
translate(-55, 200);
cone(20,14,23,11);
ellipsoid(8, 8, 8);
pop();

//planet big
translate(-755, 300);
fill(255,200,255);
sphere(300,40,16);

//planet medium
translate(1200, -510);
fill(105,50,105);
sphere(44,40,16);
}

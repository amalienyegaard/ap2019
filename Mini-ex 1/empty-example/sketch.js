function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
}

function draw() {
  background(random(255), 50, 90);
  stroke('rgb(0,255,0)');
  strokeWeight(3);
fill(color(0, 0, 255));
rect(20, 20, 60, 60);
  rotateX(frameCount * 0.2);
  rotateY(frameCount * 0.2);
  box(200, 200, 200);
}

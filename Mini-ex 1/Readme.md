# AP2019 Mini-ex1

**Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)**

My first independent coding process was confusing, though exciting. I went through the references and read about a the syntax. I started with the function setup: The width and height of the window. I would like it to fill out the window. 

Then I went to function draw: What colour would I like the background to be? I thought it would be cool with a lot of different colours and decided to choose random colour, focusing on the red/blue/purple. I ended up drawing inspiration from the Doctor Who’s Tardis, which (I think) can be seen with the square’s rotation. The way the Tardis travels through time and space is similar to the rotation. I couldn’t find a way for it to bounce back and forth and I wish I knew how to create a cloud-like structure in the background or cogwheels. There was a lot of bumps on the way and sometimes when I tried to implement other things, it went blank and then I just had to go back to the “original” code. 



**How your coding process is differ or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)**

I think that there is a lot of similarity between my coding process and reading/writing text. First of all when you are reading a difficult text, then you might have to read it over and over again until you can understand it. If there is something that you don’t understand or read fully, you could be misunderstanding something or writing code wrong, which would mean that it wouldn’t work. Secondly, when constructing a sentence in Danish or English, you have to build the sentence in the right way, if not the receiver won’t be able to understand what you are saying. The same thing is relevant when it comes to writing code. If the code is not written in the right way, then program won’t be able to understand/convert what you have written. Structure is very important in both cases! One thing that differs the two, is that you could have an idea about what the text is about when ex. Reading fast, because there you would have an overall understanding of, maybe, the outcome/what the text is about. I don’t believe that I could read a text of coding fast and then imagine what it would look like when converted


**What is code and coding/programming practice means to you?**

I hope to be skilled in this area and I already like it, because I like experimenting and being creative, even though it means that there is a chance that I would be frustrated and on the brink of giving up. I “just” have to remind myself about the possible outcome and that this is a new thing that I am learning, and I will fail multiple times before I succeed.


![Screenshot](Skærmbillede 2019-02-08 kl. 12.59.54.png)

![Screenshot](Skærmbillede 2019-02-08 kl. 12.31.59.png)

URL: https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%201/empty-example/index.html


*By Amalie Nyegaard Mortensen,*
All my mini-ex in AP 2019
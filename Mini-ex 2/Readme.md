# AP2019 Mini_ex2

**Describe your program and what you have used and learned**


For my first emoji "Mental Health", I have used the syntax "ellipse" to create a round head. For the mouth, I have used the syntax "line", because I wanted the mouth to be neutral and you can see that the emoji is uncomfortable. The eyes are created by "ellipses" too and so are the pupils. When creating the cloud I tried out a few ideas. My first idea was that I wanted it to move. But I thought it became difficult with the different shapes that the cloud is made of. Then I decided to use the syntax "arc" at the bottom of the cloud and to shake it up, I then chose "ellipse" again. I wanted to create a red-ish shadow around the eyes using "stroke" so that you can see that the emoji is not feeling well. I chose to use the classic emoji color, yellow, because I wanted it to be neutral and useful for all, regardless of our skin color.

For my second emoji "Niqab", I have used the syntax "rect" to create the body. I decided to make the edges soft. The head shape is "ellipse" and so are the eyes and pupils. Once again I chose to use the yellow color for the face, for the same reasons stated above. The color of the Niqab is black because it is a neutral color. 

I have learned to create a cloud, I might have done it in a difficult way and maybe I will find an easier way. I have also practiced and learned about creating faces and bodies using syntaxes such as "ellipse", arc and rest.


**My emojis in a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond**


So.. I had a lot of thoughts on which subjects I wanted to touch. Mental health is still difficult to talk about, even though it affects so many people worldwide. Everybody knows a person who battles with mental health issues and it can be difficult to talk about, even with our closest friends. Not everyone recognizes it as a disease, unfortunately, and I believe that the more we talk about it, the more "it is okay not to be okay". Gladly, we are moving in the right direction and it is becoming less tabu to talk about having a diagnosis or feeling down. The cloud above the face reflects on how it is a hidden disease and it symbolizes the inner gloomy weather. 

Another issue that I find very relevant at the moment, is the Danish Ban on Face-Covering Garments that was passed on the 18th of August 2018. My personal beliefs are that no one should decide what I should wear. In a country where we treasure our freedom of speech, I find it very hypocritical that a law concerning the way we dress has been passed. Some may have the idea that Muslim women wearing niqabs are repressed, but it doesn't believe that anybody can talk on her behalf. You can't look at someone and then conclude that she must be "repressed" because of her clothing. One of the consequences of the ban is that we force women to stay at home, because of their beliefs.  By doing that we exclude them from our society by giving them fines when being in the public. It is not only niqabs that are now illegal, but also dressing up as Santa Claus, The Ninja Turtles, and Jason for Halloween. The difference between the examples is that those are costumes, where women wearing a niqab is doing it because they believe it is encouraged by their faith. 


"Mental Health" emoji URL1: https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%202/empty-example/index3.html

![Screenshot](Skærmbillede 2019-02-15 kl. 11.32.46.png)

"Niqab" emoji URL2: https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%202/empty-example/index2.html

![Screenshot](Skærmbillede 2019-02-15 kl. 11.32.52.png)

/anden emoji Mental health til mandag d. 18 februar */
function setup() {
  createCanvas(800, 800);
}
function draw() {
  background(255);

  //ansigtsform
  fill(255,255,0);
  ellipse(200,250,200,200);

	//mund
	stroke(0)
	line(170, 265, 225, 265);

  //venstre øje
	stroke (255,175,0)
  fill(255);
  ellipse(160,220,38,32);

	//venstre øje - pupil
  fill(0);
  ellipse(160,220,8,8c

  //højre øje
  fill(255);
  ellipse(235,220,38,32);

	// højre øje - pupil
  fill(0);
  ellipse(235,220,8,8);

	//små skyer i hver side, første er højre

	fill(215);
	noStroke(255);
	ellipse (80, 80, 60)

	ellipse (300, 80, 60)

	//3 store skyer øverst i midten

	ellipse (130, 80, 90)

	ellipse (190, 70, 90)

	ellipse (250, 80, 90)

	//3 skyer i bunden

	arc(120, 100, 80, 70, 0, PI + QUARTER_PI, CHORD);

	arc(190, 100, 80, 80, 0, PI + QUARTER_PI, CHORD);

	arc(260, 100, 80, 70, 0, PI + QUARTER_PI, CHORD);
}

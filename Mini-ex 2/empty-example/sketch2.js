/første emoji Niqab til mandag d. 18 februar */
function setup(){
  createCanvas(200, 200);
  rectMode(CENTER);
  }

  function draw(){

//krop
rect(100, 150, 75, 95, 20, 15, 10, 5);

//ansigtsform
  fill(0);
  ellipse(100,70,80,80);

//øjenhul
  fill(255,255,0);
  rect(100,60, 65, 17, 5);

//venstre øje
  fill(255);
  ellipse(85,60,18,12);
//pupil
  fill(0);
  ellipse(85,60,5,5);

//højre øje
  fill(255);
  ellipse(115,60,18,12);
//pupil
  fill(0);
  ellipse(115,60,5,5);


  }

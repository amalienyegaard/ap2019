**AP2019 Mini-ex 4**


I have made a program that is supposed to generate *one* pokémon that "looks" like you when you pressed the button "generate pokémon". I chose to capture the user of my program with the syntaxes ctracker and button.mousePressed. My theme for the program was Pokémon. My program was suppose to capture and compare humans and pokémons and by that draw inspiration from the Transmediale theme "Capture all", because what first came to my mind was "Gotta catch 'em all" (the english slogan and the theme song from Pokémon the animated series). To this day Pokémon is still extremly popular/iconic and has a global reach. The storyline is capturing pokémons and that, in my opinion, went great togehter with this weeks theme.

I had a couple of ideas and when it came to some I had to give up on (for now). I wanted my program to generate one random picture of a pokémon (from my pre-selected pokémons) and then "compare" it to the user of my program. Unfortunately, it did not work. Instead, my program generates a lot of pokémons when pressing the button "generate pokémon" with the mouse. I also would have liked the theme song to be played and the button be a pokéball. 

This was the first time where my patience really got challenged - I had many ideas but not the knowledge to execute them (YET). 


[Runme](https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%204/empty-example/index4.html)

![Screenshot](generate.png) 

**AP2019 Mini-ex 6**

I had high hopes about this weeks mini ex and I find the theme in general very exciting. I tried out with the different sprites in the "assets" folder and found the one that I would like to use in my program. After that I tried to create a background with an image of space, but it did not work like I wanted to. I was hoping to be sure in my process, so that I would end up creating a game. I imagined a game where the you have to protect the sun for objects flying through space or make sure the sun does not die.

Once I tried making adjustments to my program, the other parts of my code stopped working. When I tried to create a class it stopped working, I tried to look at the console, but that did not answer the problem. The same things happened over and over. The code that is left is the only code that would work. So unfortunately I did not fulfill this weeks assignment. Thankfully I will learn from this and keep trying, eventhough I am frustrated about how it worked out in the end.


[Runme](https://cdn.staticaly.com/gl/amalienyegaard/ap2019/raw/master/Mini-ex%206/empty-example/index6.html)
![Screenshot](miniex6.png)

According to Fuller and Goffey's text "The obscure Objects of Object Orientation.", the characteristics of object-oriented programming is about modelling behavior with and of objects. It is also about strictly defined ordering of routines and subroutines. OOP is about structure/organizing code and class with details/overview on the deifinition of an object (templace/blueprint).

Refference:
Fuller, M & Goffey, A. "The obscure Objects of Object Orientation." How to be a geek : essays on the culture of software. Eds Matthew Fuller. Cambridge, UK, Malden, MA, USA : Polity, 2017 

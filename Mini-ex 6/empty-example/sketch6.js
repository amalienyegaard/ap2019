var sun;

function preload() {
  sun = loadAnimation('assets/sun1.png', 'assets/sun3.png');
}

function setup() {
  createCanvas(1000, 790);
}

function draw() {
  background(0, 0, 55);

//sun placement
  animation(sun, 520, 350);

//planet to the left
  ellipse(200, 90, 100);
  fill(105,0,105);
//planet to the right
  ellipse(870, 606, 275);
}
